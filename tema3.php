<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Parcial 2 - Tema 3</title>
</head>

<body>
    <?php

define('TAMANO_MATRIZ', 3);

function generarMatrizAleatoria($tamano) {
    $matriz = array();
    for ($i = 0; $i < $tamano; $i++) {
        for ($j = 0; $j < $tamano; $j++) {
            $matriz[$i][$j] = rand(0, 9);
        }
    }
    return $matriz;
}

function imprimirYCalcularSumaDiagonal($matriz) {
    $sumaDiagonal = 0;
    echo "Matriz: <br>";
    for ($i = 0; $i < count($matriz); $i++) {
        for ($j = 0; $j < count($matriz[$i]); $j++) {
            echo $matriz[$i][$j] . "\t";
            if ($i == $j) {
                $sumaDiagonal += $matriz[$i][$j];
            }
        }
        echo "<br>";
    }
    echo "Suma de la diagonal principal: $sumaDiagonal<br>";
    return $sumaDiagonal;
}

function verificarCondicionSuma($suma) {
    if ($suma >= 10 && $suma <= 15) {
        echo "¡Condición cumplida! Suma de la diagonal entre 10 y 15. Suma: $suma.<br> Fin del script. <br>";
        exit;
    } else {
        echo "La suma de la diagonal principal no está entre 10 y 15. Continuando...<br><br>";
    }
}

function main() {
    while (true) {
        $matriz = generarMatrizAleatoria(TAMANO_MATRIZ);
        $sumaDiagonal = imprimirYCalcularSumaDiagonal($matriz);
        verificarCondicionSuma($sumaDiagonal);
    }
}

main();
?>


</body>

</html>